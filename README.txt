Water-Rights Scenario 

Description:
-----------------------------
Water-rights scenario addition to the CWU Center for Spatial Information 
Kittitas Water Rights Mapping Project. Feature allows the user to create
scenarios for water-rights within the Kittitas County by selecting specific
values that affect the climate and water level. 

Group Members:
-----------------------------
Team Lead:         Yosef Gamble
Design Lead:       Michael Stickel
User Interface:    Alex Romano
Database Lead:     Zayne Betts
Quality Assurance: Erin Palmer

Faculty Advisor:   Dr. Ed Lulofs
Client:            Dr. Michael Pease