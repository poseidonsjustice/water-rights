<?php

//object simYear containing  months
class SimYear{
  protected $months = array("January", "February", "March", "April", "May", "June", "July", "August",
      "September", "October", "November", "December");
  protected $years = array(
  0 => array(
    "January" => 1,
    "February" => 1,
    "March" => 1,
    "April" => 1,
    "May" => 1,
    "June" => 1,
    "July" => 1,
    "August" => 1,
    "September" => 1,
    "October" => 1,
    "November" => 1,
    "December" => 1, 
    )
  );
  /*
  Function that sets the base year of this simulation
  $base: The year array sent
  */
  public function setBaseYear($base){
    $this->years[0] = $base;
  }
  
  /*
  Function that adds a predicted year to the end of the array
  $predictedYear: The predicted year array
  */
  public function addYear($predictedYear){
    $this->years[sizeof($this->years)] = $predictedYear;
  }
  
  /*
  Function that returns this simulation object
  */
  public function getSim(){
    return $this->years;
  }
  
  /*
  Function that gets the water level for a given month name
  */
  public function getMonth($monthName){
    return $this->years[0][$monthName];
  }
  
  /*
  Function that gets a given year in this simulation
  */
  public function getYear($index){
    return $this->years[$index];
  }
  
  /*
  Function that gets the name of the months in the month array
  */
  public function getMonthName($index){
    return $this->months[$index];
  }
}
?>