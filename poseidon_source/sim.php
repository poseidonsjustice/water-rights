<?php

// Values from HTML form
$years = $_POST['year'];
$waterLevel = $_POST['slider'];
$index = validateClimate();

/* Checks if climate value is null. 
 * Returns a default value [random] if true
 */
function validateClimate() {
  if (empty($_POST['climate'])) {
    return "random";
  } else {
    return $_POST['climate'];
  }
}

/* Calls the given simulation
 * index: Index of the simulation array [Dry, Wet, Predicted, Random]
 * waterLevel: Selected percentage for water level [20% - 200%]
 * years: Number of years in time scenario [1, 2, 3, 4, 5, 10]
 */
function simulate($years, $waterLevel, $index) {
  switch ($index) {
    case "wet":
      include 'simulationWet.php';
      simulateWet($years, $waterLevel);
      break;
    case "predicted":
      include 'simulationPredicted.php';
      simulatePredicted($years, $waterLevel);
      break;
    case "random":
      include 'simulationRandom.php';
      simulateRandom($years, $waterLevel);
      break;
    case "dry":
      include 'simulationDry.php';
      simulateDry($years, $waterLevel);
      break;
  }
}

// Runs simulation
simulate($years, $waterLevel, $index);

?>